import Serializer from 'lance/serialize/Serializer';
import DynamicObject from 'lance/serialize/DynamicObject';
import PixiRenderableComponent from 'lance/render/pixi/PixiRenderableComponent';
import Renderer from '../client/SpaaaceRenderer';

export default class Missile extends DynamicObject {

    constructor(gameEngine, options, props, typeId, geometrieId) {
        super(gameEngine, options, props);
        this.typeId = typeId;
        this.geometrieId = geometrieId;
    }

    // this is what allows usage of shadow object with input-created objects (missiles)
    // see https://medium.com/javascript-multiplayer-gamedev/chronicles-of-the-development-of-a-multiplayer-game-part-2-loops-and-leaks-10b453e843e0
    // in the future this will probably be embodied in a component

    static get netScheme() {
        return Object.assign({
            inputId: { type: Serializer.TYPES.INT32 },
            typeId: { type: Serializer.TYPES.STRING },
            geometrieId: { type: Serializer.TYPES.STRING },
        }, super.netScheme);
    }

    onAddToWorld(gameEngine) {
        let renderer = Renderer.getInstance();
        if (renderer) {
            let sprite = this.getMissileSprit(this.typeId);
            renderer.sprites[this.id] = sprite;
            this.setMissileProperties(this.geometrieId, sprite);
            sprite.anchor.set(0.5, 0.5);
            sprite.position.set(this.position.x, this.position.y);
            renderer.layer2.addChild(sprite);
        }
    }

    getMissileSprit(typeId) {
        if (typeId === '1') {
            return new PIXI.Sprite(PIXI.loader.resources.missileKarout.texture);
        } else if (typeId === '2') {
            return new PIXI.Sprite(PIXI.loader.resources.missileZobi.texture);
        } else {
            return new PIXI.Sprite(PIXI.loader.resources.missile.texture);
        }
    }

    setMissileProperties(geometrieId, sprit) {
        var width = 0;
        var height = 0;
        if (geometrieId === '1') {
            width = 81;
            height = 46;
        } else if (geometrieId === '2') {
            width = 81 * 0.3;
            height = 46 * 0.3;
        } else {
            width = 81 * 0.5;
            height = 46 * 0.5;
        }
        sprit.width = width;
        sprit.height = height;
    }

    onRemoveFromWorld(gameEngine) {
        let renderer = Renderer.getInstance();
        if (renderer && renderer.sprites[this.id]) {
            renderer.sprites[this.id].destroy();
            delete renderer.sprites[this.id];
        }
    }

    syncTo(other) {
        super.syncTo(other);
        this.inputId = other.inputId;
    }
}
