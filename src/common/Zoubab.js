var Zoubab = {};

Zoubab.getZoubabIdFromDOM = function (domId) {
    if (domId === 'bot') {
        return '0';
    } else if (domId === 'karout') {
        return '1';
    } else if (domId === 'zobi') {
        return '2';
    }
}

Zoubab.getDefaultPritPath = function () {
    return 'assets';
}

Zoubab.getDefaultFileExtension = function () {
    return 'gif';
}

Zoubab.getAllZoubab = function () {
    return [
        {
            id: '0',
            name: 'bot',
            spritPath: `${this.getDefaultPritPath()}/ship.png}`,
            missileId: '0',
            geometrieId: '0',
        },
        {
            id: '1',
            name: 'karout',
            spritPath: `${this.getDefaultPritPath()}/karout.${this.getDefaultFileExtension()}`,
            missileId: '1',
            geometrieId: '1',
        },
        {
            id: '2',
            name: 'zoubi',
            spritPath: `${this.getDefaultPritPath()}/zobi.${this.getDefaultFileExtension()}`,
            missileId: '2',
            geometrieId: '2',
        },
    ]
}

export default Zoubab;